# ライブ配信(ビデオ配信)システム

## 1.概要
* 視聴者がWebRTCかHLSで接続する2種類のサーバーを用意
    * なお、配信者はWebRTC接続のみ
* 両者のメリット/デメリットを勘案し、どちらかを採用すること

## 2.共通仕様
### 2.1.配信サーバー
* 配信サーバーはNode.js(Typescript)で開発されている
* 配信サーバーはTURNを兼ねている
    * TURNとは、NAT(ルーター)の先にある端末と通信するためのサーバー
    * なお、STUNは無料で公開されているgoogleのサーバーを使用

### 2.2.配信者
* 配信者と配信サーバーはWebRTCで接続する
* 配信者と配信サーバーの接続にFirebaseを仲介してシグナリングする
* WebRTCはUDPプロトコルなのでCDNには乗らない

### 2.3.コスト
* Firebase  
https://firebase.google.com/docs/firestore/billing-example?hl=ja  
小規模: アプリ インストール数 50,000（1 日のアクティブ ユーザー数 5,000）の場合: $12.14/月  
中規模: アプリ インストール数 1,000,000（1 日のアクティブ ユーザー数 100,000）の場合: $292.02/月  
大規模: アプリ インストール数 10,000,000（1 日のアクティブ ユーザー数 1,000,000）の場合: $2,951.52/月  

## 3.WebRTC接続
### 3.1.メリット
* WebRTCなのでリアルタイム配信(遅延2秒以内)が可能。よって双方向のやり取り(会話)が可能  
例: Zoom ( https://www.zoom.com )

### 3.2.デメリット
* 配信サーバーの負荷が高く、視聴者が増えるほどサーバーの性能を上げなくてはいけないのでランニングコストが高い

### 3.3.構成
```plantuml
@startuml
actor 配信者A
card アプリA
cloud GoogleCloud {
  agent 配信サーバー<<Node.js>>
  database Firebase
}
card アプリA1
card アプリA2
actor 視聴者A1
actor 視聴者A2
配信者A --> アプリA: 1.配信
アプリA --> Firebase: 2.シグナリング
Firebase --> 配信サーバー: 3.シグナリング
アプリA <---> 配信サーバー: 4.WebRTC
アプリA1 --> Firebase: 5.シグナリング
アプリA2 --> Firebase: 5.シグナリング
Firebase --> 配信サーバー: 6.シグナリング
配信サーバー --> アプリA1: 7.WebRTC
配信サーバー --> アプリA2: 7.WebRTC
アプリA1 --> 視聴者A1: 8.視聴
アプリA2 --> 視聴者A2: 8.視聴
@enduml
```

### 3.4.コスト
* 配信サーバー  
https://cloud.google.com/  
配信者1人視聴者100人につき  
CPU:8コア メモリ:7.25GB  
ディスク:10GB OS:Ubuntu24.04LTS  
1時間$0.256(約27.44円)  
※ 視聴者100人の目安。視聴者上限によってサーバーコストは変わってくる  

### 3.5.コスト削減案
* 問題点
    * 配信サーバーを起動しっぱなしだとランニングコストが高い
* 削減案  
    1. 配信者が配信するときだけ配信サーバーを起動する
        * 配信サーバーを起動するのに1分程度かかるので、配信者が1分待てるか？
        * ランニングコストが安い
    1. 混む時間帯・空いてる時間帯を予想して稼働する配信サーバーを調整する
        * 例えば毎日19時〜24時は配信サーバーを増やし、深夜は配信サーバーを減らす、など

## 4.HLS接続
### 4.1.メリット
* HLSなのでCDNに乗る。よって視聴者が増えても配信サーバーの負荷が低く、配信サーバーの性能を抑えられるのでランニングコストが安く済む  
例: Youtube Live ( https://www.youtube.com/live )

### 4.2.デメリット
* 最速でも4秒程度の遅延が発生する。CDNを伝播することで遠い端末はさらに遅延する

### 4.3.構成
```plantuml
@startuml
actor 配信者A
card アプリA
cloud GoogleCloud {
  agent 配信サーバー<<Node.js>>
  agent ロードバランサー<<Node.js>>
  database Firebase
}
card アプリA1
card アプリA2
actor 視聴者A1
actor 視聴者A2
配信者A --> アプリA: 1.配信
アプリA --> Firebase: 2.シグナリング
Firebase --> 配信サーバー: 3.シグナリング
アプリA <---> 配信サーバー: 4.WebRTC
配信サーバー --> ロードバランサー: 5.HTTP
ロードバランサー --> アプリA1: 6.HLS
ロードバランサー --> アプリA2: 6.HLS
アプリA1 --> 視聴者A1: 7.視聴
アプリA2 --> 視聴者A2: 7.視聴
@enduml
```

### 4.4.コスト
* 配信サーバー  
https://cloud.google.com/  
配信者1人につき  
CPU:1コア メモリ:1.0GB  
ディスク:10GB OS:Ubuntu24.04LTS  
月額$24.82(約2600円)  

* CDN  
SD(480P)のバイトサイズ: 約1分25MB  
(参考URL)  
https://videcheki.com/%E5%8B%95%E7%94%BB-%E7%94%BB%E8%B3%AA-sd-hd-%E3%83%95%E3%83%ABhd/  
https://digital-faq.olympus.co.jp/faq/public/app/servlet/qadoc?QID=003745  
25MB*225分*1000人*30日=168,750GB  
https://cloud.google.com/cdn/pricing?hl=ja  
10GB*$0.09+140GB*$0.06+18GB*$0.05=$10.2(約1140円)  
※ 配信中継(CDN)は受信者（ユーザー数）と配信の動画容量によって変わってきます。  
　例えば、SD(480P)だと 約1分25MBになります。  
　それが1日配信225分だとした場合、1ヶ月分(30日)をユーザー1000人に配信するとして、約1140円かかる計算です。 

# P2P(WebRTC)
## 1.概要
* 配信サーバーを使わず、直接配信者から視聴者へ配信する仕組み

## 2.メリット
* 配信サーバーを使わないので、配信サーバーのコストがかからない
    * ただし、ほとんどのケースでTURNが必要なのでTURNのランニングコストは必要

## 3.デメリット
* 視聴者数が配信者の端末の性能と通信回線の速度に制限される
   * 現状視聴者6人程度が限界

### 4.構成
```plantuml
@startuml
actor 配信者A
card アプリA
cloud GoogleCloud {
  agent TURN
  database Firebase
}
card アプリA1
card アプリA2
actor 視聴者A1
actor 視聴者A2
配信者A --> アプリA: 1.配信
アプリA1 --> Firebase: 2.シグナリング
Firebase --> アプリA: 3.シグナリング
アプリA --> TURN: 4.WebRTC
TURN --> アプリA1: 5.WebRTC
アプリA1 --> 視聴者A1: 6.視聴
アプリA2 --> Firebase: 7.シグナリング
Firebase --> アプリA: 8.シグナリング
アプリA --> TURN: 9.WebRTC
TURN --> アプリA2: 10.WebRTC
アプリA2 --> 視聴者A2: 11.視聴
@enduml
```

# 投げ銭システム

## 1.概要
* アプリ内通貨の管理にFirebaseを使用
* 決済はWebアプリ(ブラウザ)はStripeを使用、iOSアプリはAppStoreを使用、AndroidアプリはPlayStoreを使用

## 2.Webアプリ(ブラウザ) - Stripe
### 2.1.手数料
* Stripe Connect ( https://stripe.com/ja-us/connect/use-cases ) を使用
* 手数料3.6% ( https://stripe.com/jp/pricing )
### 2.2.構成
* 視聴者から課金された収入はStripe内にプールされる
* 配信者から換金リクエストに応じて、Stripe内にプールされた収入から配信者へ支払われる
* 運営者の収入もStripe内にプールから引き出す
```plantuml
@startuml
actor 視聴者A1
card アプリA1
cloud GoogleCloud {
  database Firebase
}
cloud Stripe {
}
card アプリA
actor 配信者A
actor 運営者
視聴者A1 --> アプリA1: 1.課金 
アプリA1 --> Stripe: 2.決済
Stripe -up-> Firebase: 3.決済情報
Firebase -up-> アプリA1: 4.アプリ内通貨
視聴者A1 --> アプリA1: 5.投げ銭
アプリA1 --> Firebase: 6.投げ銭
Firebase --> アプリA: 7.投げ銭
配信者A --> アプリA: 8.換金命令
アプリA --> Firebase: 9.換金命令
Firebase --> Stripe: 10.支払い命令
Stripe --> 配信者A: 11.銀行振込
Stripe --> 運営者: 収入
@enduml
```

## 3.iOSアプリ - AppStore
### 3.1.手数料
* 手数料 前年の収益が100万ドル以内であれば15%、それを超えると30%  
https://www.apple.com/jp/newsroom/2020/11/apple-announces-app-store-small-business-program/

### 3.2.構成
* 視聴者から課金された収入はAppStore内にプールされる
* 配信者から換金リクエストには、運営者がAppStoreから出金し、自ら配信者へ支払う必要がある
* 運営者の収入もAppStore内のプールから引き出す
```plantuml
@startuml
actor 視聴者A1
card アプリA1
cloud GoogleCloud {
  database Firebase
}
cloud AppStore {
}
card アプリA
actor 配信者A
actor 運営者
視聴者A1 --> アプリA1: 1.課金 
アプリA1 --> AppStore: 2.決済
AppStore -up-> Firebase: 3.決済情報
Firebase -up-> アプリA1: 4.アプリ内通貨
視聴者A1 --> アプリA1: 5.投げ銭
アプリA1 --> Firebase: 6.投げ銭
Firebase --> アプリA: 7.投げ銭
配信者A --> アプリA: 8.換金命令
アプリA --> Firebase: 9.換金命令
Firebase --> 運営者: 10.支払い命令
運営者 --> AppStore: 11.出金
AppStore --> 運営者: 12.入金
運営者 --> 配信者A: 13.銀行振込
@enduml
```

## 4.Androidアプリ - PlayStore
### 4.1.手数料
* 年間収益が 100 万ドル（USD）までの場合15%、100 万ドルを超える場合30%  
https://support.google.com/googleplay/android-developer/answer/112622?hl=ja

### 4.2.構成
iOSアプリと同じ構成なので省略

## 5.手数料軽減案
* 問題点
    * iOS/Androidアプリの課金手数料が高い
* 軽減案
    1. iOS/Androidアプリで課金しない
        * Webアプリのみで課金できるようにする
            * ただし、iOS/AndroidアプリからWebアプリへの誘導は禁止されている
    1. iOS/Androidアプリのアプリ内通貨レートを課金手数料分悪くする(視聴者から課金手数料分を徴収する)

## 6.収入集約案
* 問題点
    * Stripe、AppStore、PlayStoreで収入のプールが違うので、配信者への換金の支払いが煩雑になる
* 集約案
    1. 全てのプールから出金して、運営者が自ら支払う
        * Stripeの支払い機能を使わず、運営者が独自で支払い業務を行う
            * 銀行振込一括CSVファイル(全銀形式)出力などの機能追加は可能  
            全銀形式: https://lp.toriders.app/blog/difference-sougoufurikomi-kyuyofurikomi
    1. AppStore、PlayStoreから出金して、Stripeに入金して、Stripeの支払い機能を使う
